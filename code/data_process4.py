import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def use_dict(df):
    numarray = []
    strarray = []
    s_array = []

    for i in range(len(df)):
        s = df[i]
        if s == '  ':
            s_array.append("RMA_Signal (Hyb_" + str(i + 1) + ")")
            continue
        if s not in strarray:
            strarray.append(s)

        numarray.append(strarray.index(s))
    print(s_array)
    print(numarray)
    return numarray, s_array


def get_labels(data_path="../dataset/Gene_Chip_Data/E-TABM-185.sdrf.txt"):
    df = pd.read_table(data_path, sep='\t')
    labels = np.array(df['Characteristics [BioSourceType]'])
    labels, s_array = use_dict(labels)
    return labels, s_array


def get_arrays(s_array, data_path="../dataset/Gene_Chip_Data/microarray.original.txt"):

    df = pd.read_table(data_path, sep='\t')
    df = df.T

    df.drop(['ProbeSet ID'], inplace=True)
    df.drop(s_array, inplace=True)
    arrays = np.array(df)
    print(len(arrays))
    print(len(arrays[0]))
    return arrays


def save_data(arrays, labels, save_path="../dataset/dataset4/origin_data/"):
    data_train, data_test, labels_train, labels_test = train_test_split(arrays, labels, test_size=0.2, random_state=42)
    np.save(save_path + "data_train", data_train)
    np.save(save_path + "data_test", data_test)
    np.save(save_path + "labels_train", labels_train)
    np.save(save_path + "labels_test", labels_test)


if __name__ == '__main__':
    labels, s_array = get_labels()
    print("get labels finished!")
    arrays = get_arrays(s_array)
    print("get data finished!")
    save_data(arrays, labels)
