"""
针对多分类问题处理数据。
使用Characteristics [BioSourceType]列作为label列。
semi-supervised
"""
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def use_dict(df):
    numarray = []
    strarray = []
    s_array = []

    for i in range(len(df)):
        s = df[i]
        if (s == '  '):
            s_array.append("RMA_Signal (Hyb_" + str(i + 1) + ")")
            continue
        if (s not in strarray):
            strarray.append(s)

        numarray.append(strarray.index(s))
    print(s_array)
    return numarray, s_array


def get_labels(data_path="../dataset/Gene_Chip_Data/E-TABM-185.sdrf.txt"):
    labels = []
    s_array = []
    df = pd.read_table(data_path, sep='\t')
    labels = np.array(df['Characteristics [BioSourceType]'])
    labels, s_array = use_dict(labels)
    return labels, s_array


def get_arrays(s_array, data_path="../dataset/Gene_Chip_Data/microarray.original.txt"):
    arrays = []

    df = pd.read_table(data_path, sep='\t')
    null = df[s_array]
    df = df.T

    df.drop(['ProbeSet ID'], inplace=True)

    unlabel = null.copy(deep=True)
    unlabel = unlabel.T

    df.drop(s_array, inplace=True)
    arrays = np.array(df)
    unlabel = np.array(unlabel)
    print(len(arrays))
    print(len(arrays[0]))
    return arrays, unlabel


def save_data(arrays, labels, unlabel, save_path="../dataset/dataset5/origin_data/"):
    data_train, data_test, labels_train, labels_test = train_test_split(arrays, labels, test_size=0.2, random_state=42)
    print(len(data_train))
    print(len(labels_test))

    # for i in range(len(data_train)):
    #     print (str(labels_train[i]) + " "+  str(data_train[i]))
    #
    # for i in range(len(data_test)):
    #     print (str(labels_test[i]) + " " + str(data_test[i]))

    np.save(save_path + "data_train", data_train)
    np.save(save_path + "data_test", data_test)
    np.save(save_path + "labels_train", labels_train)
    np.save(save_path + "labels_test", labels_test)
    np.save(save_path + "unlabel", unlabel)


if __name__ == '__main__':
    labels, s_array = get_labels()
    print("get labels finished!")
    arrays, unlabel = get_arrays(s_array)
    print("get data finished!")
    save_data(arrays, labels, unlabel)
