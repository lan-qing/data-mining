import sys

sys.path.insert(0, "../../")
import numpy as np
import sklearn
import time
from sklearn.linear_model import LogisticRegression

data_p = "../../dataset/dataset"
# dataset = "1"
dataset = "4"
load_d_f = "/pca/"
load_l_f = "/origin_data/"
ncompo = [400, 2000]

p = data_p + dataset
load_data_path = p + load_d_f
load_label_path = p + load_l_f
s = str(ncompo[0])
train_data = "pca_data_train_" + s + ".npy"
test_data = "pca_data_test_" + s + ".npy"

multi = "multinomial"
solver_a = ["sag", "newton-cg", "lbfgs", "liblinear"]
solver = solver_a[2]


def load_pca_data(data_path=load_data_path):
    pca_data_train = np.load(data_path + train_data)
    pca_data_test = np.load(data_path + test_data)
    return pca_data_train, pca_data_test


def load_labels(data_path=load_label_path):
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return labels_train, labels_test


def lr_model(train_data, train_labels):
    print("lr fits start!")
    start_time = time.time()
    model = LogisticRegression(solver=solver, multi_class=multi)
    model.fit(train_data, train_labels.ravel())
    end_time = time.time()
    predicted = model.predict(train_data)
    score = sklearn.metrics.accuracy_score(train_labels, predicted)
    f1_score = sklearn.metrics.f1_score(train_labels, predicted, average='micro')
    print("lr fits successfully! time {0} train acc {1} f1_score {2}".format(end_time - start_time, score, f1_score))
    return model


def lr_test(model, test_data, test_labels):
    predicted = model.predict(test_data)
    score = sklearn.metrics.accuracy_score(test_labels, predicted)
    f1_score = sklearn.metrics.f1_score(test_labels, predicted, average='micro')
    print("test acc {0} f1_score {1}".format(score, f1_score))


if __name__ == '__main__':
    pca_data_train, pca_data_test = load_pca_data()
    labels_train, labels_test = load_labels()
    model = lr_model(pca_data_train, labels_train)
    lr_test(model, pca_data_test, labels_test)
