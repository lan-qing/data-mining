import os
import time

import tensorflow as tf
import numpy as np
from sklearn import metrics


class DeepModel:
    def __init__(self):
        self.input_data = None
        self.output_data = None
        self.loss = None
        self.train_step = None
        self.expected_output = None
        self.keep_prob = None

        self.saver = None
        self.build()
        self.init = None
        self.sess = tf.Session()

        self.paras = None
        self._path = "../../dataset/dataset1/deep_model/"

    @property
    def save_path(self):
        if self._save_path is None:
            save_path = '%s/checkpoint' % self._path
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            save_path = os.path.join(save_path, 'model.ckpt')
            self._save_path = save_path
        return self._save_path

    @property
    def logs_path(self):
        if self._logs_path is None:
            logs_path = '%s/%s' % (self._path, str(time.time()))
            if not os.path.exists(logs_path):
                os.makedirs(logs_path)
            self._logs_path = logs_path
        return self._logs_path

    def load_model(self):
        try:
            self.saver.restore(self.sess, self.save_path)
        except Exception:
            raise IOError('Failed to load model from save path: %s' % self.save_path)
        print('Successfully load model from save path: %s' % self.save_path)

    def save_model(self, global_step=None):
        self.saver.save(self.sess, self.save_path, global_step=global_step)

    def build(self):
        self.input_data = tf.placeholder(tf.float32, shape=[None, 2000])
        self.expected_output = tf.placeholder(tf.float32, shape=[None, 1])
        self.keep_prob = tf.placeholder(tf.float32, shape=[])

        l1 = tf.layers.dense(self.input_data, 256, tf.nn.relu)
        l1_drop = tf.nn.dropout(l1, self.keep_prob)
        l2 = tf.layers.dense(l1_drop, 32, tf.nn.relu)
        l2_drop = tf.nn.dropout(l2, self.keep_prob)
        self.output_data = tf.layers.dense(l2_drop, 1, tf.nn.sigmoid)
        optimizer = tf.train.AdamOptimizer(5e-4)

        self.loss = tf.losses.mean_squared_error(labels=self.expected_output, predictions=self.output_data)
        self.train_step = optimizer.minimize(self.loss)

        self.saver = tf.train.Saver()

    def fit(self, data, labels):
        print(data.shape, labels.shape)
        paras = {
            self.input_data: data,
            self.expected_output: np.reshape(labels, [-1, 1]),
            self.keep_prob: 0.5
        }
        print("Start fitting")
        self.init = tf.global_variables_initializer()
        self.sess.run(self.init)
        _, data_test = load_pca_data()
        _, label_test = load_labels()
        for step in range(500):
            loss, _ = self.sess.run(fetches=[self.loss, self.train_step], feed_dict=paras)

            if True:
                pass
                deep_test(self, data_test, label_test)
            print("step ", step, loss)
        # self.save_model()

    def predict(self, data):
        paras = {
            self.input_data: data,
            self.keep_prob: 1
        }
        output_data = self.sess.run(fetches=self.output_data, feed_dict=paras)
        return output_data


dataset = 1
pca = ""
average = "micro"


def load_pca_data(data_path="../../dataset/dataset" + str(dataset) + "/auto_encoder/"):
    pca_data_train = np.load(data_path + "auto_encode_train" + str(pca) + ".npy")
    pca_data_test = np.load(data_path + "auto_encode_test" + str(pca) + ".npy")
    return pca_data_train, pca_data_test


def load_labels(data_path="../../dataset/dataset" + str(dataset) + "/origin_data/"):
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return labels_train, labels_test


def deep_model(train_data, train_labels):
    print("Deep model fits start!")
    start_time = time.time()
    model = DeepModel()
    model.fit(train_data, train_labels)
    end_time = time.time()
    predicted = model.predict(train_data)
    zo_predicted = predicted > 0.5
    score = metrics.accuracy_score(train_labels, zo_predicted)
    f1_score = metrics.f1_score(train_labels, zo_predicted, average=average)
    print("Deep model fits successfully! time {0} train acc {1} f1_score {2}".format(end_time - start_time, score,
                                                                                     f1_score))
    return model


def deep_test(model, test_data, test_labels):
    predicted = model.predict(test_data)
    zo_predicted = predicted > 0.5
    score = metrics.accuracy_score(test_labels, zo_predicted)
    f1_score = metrics.f1_score(test_labels, zo_predicted, average=average)
    print("test acc {0} f1 {1}".format(score, f1_score))


if __name__ == '__main__':
    pca_data_train, pca_data_test = load_pca_data()
    labels_train, labels_test = load_labels()
    model = deep_model(pca_data_train, labels_train)
    deep_test(model, pca_data_test, labels_test)
