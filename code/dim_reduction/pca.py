import sys

sys.path.insert(0, "../../")
import numpy as np
from sklearn.decomposition import PCA

data_p = "../../dataset/dataset"
dataset = "4"
load_f = "/origin_data/"
save_f = "/pca/"
ncompo = [400]
ncompo_s = ncompo[0]

p = data_p + dataset
load_path = p + load_f
save_path = p + save_f


def load_data(data_path=load_path):
    data_train = np.load(data_path + "data_train.npy")
    data_test = np.load(data_path + "data_test.npy")
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return data_train, data_test, labels_train, labels_test


def pca(data_train, data_test):
    print("start pca")
    pca = PCA(n_components=ncompo_s)
    pca.fit(data_train)
    pca_data_train = pca.transform(data_train)
    pca_data_test = pca.transform(data_test)
    print(sum(pca.explained_variance_ratio_))
    return pca_data_train, pca_data_test


def save_data(data, data_name, data_path=save_path):
    np.save(data_path + data_name, data)


if __name__ == '__main__':
    data_train, data_test, labels_train, labels_test = load_data()
    # assert data_train.shape == (4716, 22283)
    # assert data_test.shape == (1180, 22283)
    # assert labels_train.shape == (4716,)
    # assert labels_test.shape == (1180,)
    pca_data_train, pca_data_test = pca(data_train, data_test)
    s = str(ncompo_s)
    save_data(pca_data_test, "pca_data_test_" + s)
    save_data(pca_data_train, "pca_data_train_" + s)
