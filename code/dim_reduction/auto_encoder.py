import os
import time

import tensorflow as tf
import numpy as np


class AutoEncoderModel:
    def __init__(self):
        self.input_data = None
        self.output_data = None
        self.loss = None
        self.train_step = None

        self.saver = None
        self.init = None

        # use tf-gpu
        gpuConfig = tf.ConfigProto(allow_soft_placement=True)
        gpuConfig.gpu_options.allow_growth = True
        self.sess = tf.Session(config=gpuConfig)

        self.paras = None
        self._save_path = "../../dataset/dataset1/auto_encoder/"

    @property
    def save_path(self):
        if self._save_path is None:
            save_path = '%s/checkpoint' % self._save_path
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            save_path = os.path.join(save_path, 'model.ckpt')
            self._save_path = save_path
        return self._save_path

    @property
    def logs_path(self):
        if self._logs_path is None:
            logs_path = '%s/%s' % (self._save_path, str(time.time()))
            if not os.path.exists(logs_path):
                os.makedirs(logs_path)
            self._logs_path = logs_path
        return self._logs_path

    def load_model(self):
        try:
            self.saver.restore(self.sess, self.save_path)
        except Exception:
            raise IOError('Failed to load model from save path: %s' % self.save_path)
        print('Successfully load model from save path: %s' % self.save_path)

    def save_model(self, global_step=None):
        self.saver.save(self.sess, self.save_path, global_step=global_step)

    def build(self, layer_sizes, expected_output_pos):
        self.input_data = tf.placeholder(tf.float32, shape=[None, 22283])
        layers = {0: self.input_data}
        for i, layer_size in zip(range(1, len(layer_sizes)), layer_sizes[1:]):
            layers[i] = tf.layers.dense(layers[i - 1], layer_size, tf.nn.relu, name="dense" + str(i))
        self.output_data = layers[len(layer_sizes) - 1]
        optimizer = tf.train.AdamOptimizer(self.paras["lr"])

        self.loss = tf.losses.mean_squared_error(labels=self.input_data, predictions=self.output_data)
        self.train_step = optimizer.minimize(self.loss)

        self.expected = layers[expected_output_pos]
        self.saver = tf.train.Saver()

    def fit(self, data, paras, batch_size=131):  # 4716 = 36 * 131
        self.paras = paras
        middle_size = int((22283 * paras["output_size"]) ** 0.5)
        print(middle_size)  # 1147
        self.build([22283, middle_size, paras["output_size"], middle_size, 22283], 2)
        self.init = tf.global_variables_initializer()
        self.local_init = tf.local_variables_initializer()
        self.sess.run(self.init)

        print("Start fitting")
        for epoch in range(100):
            loss = None
            for step in range(int(4716 / batch_size)):
                data_tmp = data[step * batch_size: (step + 1) * batch_size]
                loss, _ = self.sess.run(fetches=[self.loss, self.train_step], feed_dict={self.input_data: data_tmp})
            print("epoch ", epoch, loss)
        self.save_model()

    def transform(self, data):
        output_data = self.sess.run(fetches=self.expected, feed_dict={self.input_data: data})
        return output_data


def load_data(data_path="../../dataset/dataset1/origin_data/"):
    data_train = np.load(data_path + "data_train.npy")
    data_test = np.load(data_path + "data_test.npy")
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return data_train, data_test, labels_train, labels_test


def save_data(data, data_name, data_path="../../dataset/dataset1/auto_encoder/"):
    np.save(data_path + data_name, data)


def auto_encode(data_train, data_test):
    print("start auto encoding")
    auto_encoder = AutoEncoderModel()
    paras = {
        "output_size": 2000,
        "lr": 1e-4
    }
    auto_encoder.fit(data_train, paras)
    auto_encode_train = auto_encoder.transform(data_train)
    auto_encode_test = auto_encoder.transform(data_test)
    return auto_encode_train, auto_encode_test


if __name__ == '__main__':
    data_train, data_test, labels_train, labels_test = load_data()
    assert data_train.shape == (4716, 22283)
    assert data_test.shape == (1180, 22283)
    assert labels_train.shape == (4716,)
    assert labels_test.shape == (1180,)
    auto_encode_train, auto_encode_test = auto_encode(data_train, data_test)
    save_data(auto_encode_train, "auto_encode_train")
    save_data(auto_encode_test, "auto_encode_test")
