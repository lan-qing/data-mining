"""
针对回归问题处理数据。
数据集: microarray.original.txt
每行为一个样本，每列为一个特征
使用[RMA_Signal (Hyb_5896)]列作为label列。
"""
import sys

import numpy as np
from sklearn.model_selection import train_test_split

sys.path.insert(0, "../")


def get_data(data_path="../dataset/Gene_Chip_Data/microarray.original.txt"):
    arrays = []
    labels = []
    with open(data_path, 'r') as fin:
        number = 0
        for line in fin:
            if number == 0:
                number += 1
                continue
            data = list(map(float, line.split("\t")[1:]))

            assert len(data) == 5896
            arrays.append(data[:-1])
            labels.append(data[-1])
    arrays = np.array(arrays)
    labels = np.array(labels)
    labels = np.reshape(labels, [-1, 1])
    return arrays, labels


def save_data(arrays, labels, save_path="../dataset/dataset2/origina_data/"):
    data_train, data_test, labels_train, labels_test = train_test_split(arrays, labels, test_size=0.2, random_state=42)
    np.save(save_path + "data_train", data_train)
    np.save(save_path + "data_test", data_test)
    np.save(save_path + "labels_train", labels_train)
    np.save(save_path + "labels_test", labels_test)


if __name__ == '__main__':
    arrays, labels = get_data()
    save_data(arrays, labels)
