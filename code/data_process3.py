"""
针对二分类问题处理数据。
使用[Sequence Type]列作为label列。
"""
import sys

sys.path.insert(0, "../")

import numpy as np
from sklearn.model_selection import train_test_split


def get_particular_labels(data_path="../dataset/Gene_Chip_Data/GPL96-15653.txt"):
    labels = []
    masks = []
    with open(data_path, 'r') as fin:
        number = 0
        for line in fin:
            if number < 17:
                number += 1
                continue
            all_labels = ["Exemplar sequence", "Consensus sequence", "Control sequence"]
            words = line.split("\t")
            assert words[5] in all_labels
            if words[5] == all_labels[2]:
                masks.append(0)
            else:
                masks.append(1)
                labels.append(all_labels.index(words[5]))
            number += 1
    labels = np.array(labels)
    masks = np.reshape(np.array(masks), [-1, 1])
    return labels, masks


def get_particular_arrays(mask, data_path="../dataset/Gene_Chip_Data/microarray.original.txt"):
    arrays = []
    with open(data_path, 'r') as fin:
        number = -1
        for line in fin:
            if number < 0:
                number += 1
                continue
            data = list(map(float, line.split("\t")[1:]))

            assert len(data) == 5896
            if mask[number] == 1:
                arrays.append(data)
            number += 1
    arrays = np.array(arrays)
    return arrays


def save_data(arrays, labels, save_path="../dataset/dataset3/origin_data/"):
    data_train, data_test, labels_train, labels_test = train_test_split(arrays, labels, test_size=0.2, random_state=42)
    np.save(save_path + "data_train", data_train)
    np.save(save_path + "data_test", data_test)
    np.save(save_path + "labels_train", labels_train)
    np.save(save_path + "labels_test", labels_test)


if __name__ == '__main__':
    labels, masks = get_particular_labels()
    print("get labels finished!")
    arrays = get_particular_arrays(masks)
    print("get data finished!")
    save_data(arrays, labels)
