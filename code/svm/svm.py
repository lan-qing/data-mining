import sys

sys.path.insert(0, "../../")
import numpy as np
import sklearn
import time
from sklearn import svm

dataset = 4
type = "ovr"
pca = 400
average = "micro"


def load_pca_data(data_path="../../dataset/dataset" + str(dataset) + "/pca/"):
    pca_data_train = np.load(data_path + "pca_data_train_" + str(pca) + ".npy")
    pca_data_test = np.load(data_path + "pca_data_test_" + str(pca) + ".npy")
    return pca_data_train, pca_data_test


def load_labels(data_path="../../dataset/dataset" + str(dataset) + "/origin_data/"):
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return labels_train, labels_test


def svm_model(train_data, train_labels, kernel="sigmoid"):
    print("svm fits start!")
    start_time = time.time()
    model = svm.SVC(kernel=kernel, decision_function_shape=type)
    model.fit(train_data, train_labels)
    end_time = time.time()
    predicted = model.predict(train_data)
    score = sklearn.metrics.accuracy_score(train_labels, predicted)
    f1_score = sklearn.metrics.f1_score(train_labels, predicted, average=average)
    print("svm fits successfully! time {0} train acc {1} f1_score {2}".format(end_time - start_time, score, f1_score))
    return model


def svm_test(model, test_data, test_labels):
    predicted = model.predict(test_data)
    score = sklearn.metrics.accuracy_score(test_labels, predicted)
    f1_score = sklearn.metrics.f1_score(test_labels, predicted, average=average)
    print("test acc {0} f1 {1}".format(score, f1_score))


if __name__ == '__main__':
    pca_data_train, pca_data_test = load_pca_data()
    labels_train, labels_test = load_labels()
    model = svm_model(pca_data_train, labels_train)
    svm_test(model, pca_data_test, labels_test)
