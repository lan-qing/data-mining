import sys

sys.path.insert(0, "../../")
import numpy as np
import sklearn
import time
from sklearn.linear_model import LinearRegression


def load_data(data_path="../../dataset/dataset2/origina_data/"):
    data_train = np.load(data_path + "data_train.npy")
    data_test = np.load(data_path + "data_test.npy")
    return data_train, data_test


def load_labels(data_path="../../dataset/dataset2/origina_data/"):
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return labels_train, labels_test


def lnr_model(train_data, train_labels):
    print("lnr fits start!")
    start_time = time.time()
    model = LinearRegression(normalize=True)
    model.fit(train_data, train_labels)
    end_time = time.time()
    predicted = model.predict(train_data)
    error = sklearn.metrics.mean_squared_error(train_labels, predicted)
    print("lnr fits successfully! time {0} train error {1}".format(end_time - start_time, error))
    return model


def lnr_test(model, test_data, test_labels):
    predicted = model.predict(test_data)
    error = sklearn.metrics.mean_squared_error(test_labels, predicted)
    print("test error {0}".format(error))


if __name__ == '__main__':
    data_train, data_test = load_data()

    labels_train, labels_test = load_labels()
    model = lnr_model(data_train, labels_train)
    lnr_test(model, data_test, labels_test)
