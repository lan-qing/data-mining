"""
针对二分类问题处理数据。
使用[Material Type]列作为label列。
"""
import sys

sys.path.insert(0, "../")

import numpy as np
from sklearn.model_selection import train_test_split


def get_labels(data_path="../dataset/Gene_Chip_Data/E-TABM-185.sdrf.txt"):
    labels = []
    with open(data_path, 'r') as fin:
        number = 0
        for line in fin:
            if number == 0:
                number += 1
                continue
            two_labels = ["organism_part", "cell_line"]
            words = line.split("\t")
            assert words[14] == "Hyb_" + str(number)
            assert words[1] in two_labels

            labels.append(0 if words[1] == "organism_part" else 1)
            number += 1
    labels = np.array(labels)
    return labels


def get_arrays(data_path="../dataset/Gene_Chip_Data/microarray.original.txt"):
    arrays = []
    with open(data_path, 'r') as fin:
        number = 0
        for line in fin:
            if number == 0:
                number += 1
                continue
            data = list(map(float, line.split("\t")[1:]))

            assert len(data) == 5896
            arrays.append(data)
    arrays = np.array(arrays).transpose()
    return arrays


def save_data(arrays, labels, save_path="../dataset/dataset1/"):
    data_train, data_test, labels_train, labels_test = train_test_split(arrays, labels, test_size=0.2, random_state=42)
    np.save(save_path + "data_train", data_train)
    np.save(save_path + "data_test", data_test)
    np.save(save_path + "labels_train", labels_train)
    np.save(save_path + "labels_test", labels_test)


if __name__ == '__main__':
    arrays = get_arrays()
    print("get data finished!")
    labels = get_labels()
    print("get labels finished!")
    save_data(arrays, labels)
