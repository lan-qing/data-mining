import numpy as np
import sklearn
from sklearn.semi_supervised.label_propagation import LabelPropagation

data_p = "../../dataset/dataset"
# dataset = "1"
dataset = "5"
load_d_f = "/pca/"
load_l_f = "/origin_data/"
ncompo = [10, 400, 2000]

p = data_p + dataset
load_data_path = p + load_d_f
load_label_path = p + load_l_f
s = str(ncompo[1])
train_data = "pca_data_train_" + s + ".npy"
test_data = "pca_data_test_" + s + ".npy"


def load_data(data_path=load_data_path):
    pca_data_train = np.load(data_path + train_data)
    pca_data_test = np.load(data_path + test_data)
    return pca_data_train, pca_data_test


def load_unlabel(data_path=load_data_path):
    unlabel = np.load(data_path + "pca_data_unlabel_" + s + ".npy")
    return unlabel


def load_labels(data_path=load_label_path):
    labels_train = np.load(data_path + "labels_train.npy")
    labels_test = np.load(data_path + "labels_test.npy")
    return labels_train, labels_test


def build(data_train, data_test, labels_train, labels_test, unlabel):
    print(data_train.shape)
    print(unlabel.shape)
    data = np.append(data_train, unlabel, axis=0)
    unlabel_list = np.array([len(unlabel) * [-1]]).reshape(2809, )
    label = np.append(labels_train, unlabel_list, axis=0)

    permutation = np.random.permutation(label.shape[0])
    data = data[permutation, :]
    label = label[permutation]

    # imp = imputation.Imputer(missing_values=0)
    # imp.fit_transform(data)
    #
    # data = StandardScaler().fit(data).transform(data)

    model = LabelPropagation().fit(data, label)
    predicted = model.predict(data_test)
    score = sklearn.metrics.accuracy_score(labels_test, predicted)
    f1_score = sklearn.metrics.f1_score(labels_test, predicted, average="micro")
    print("test acc {0} f1 {1}".format(score, f1_score))


if __name__ == '__main__':
    data_train, data_test = load_data()
    labels_train, labels_test = load_labels()
    unlabel = load_unlabel()
    build(data_train, data_test, labels_train, labels_test, unlabel)
