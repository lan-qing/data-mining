### final project for data-mining

- Please put dataset *Gene_Chip_Data* into directory *dataset/*.

- And run *code/data_process.py* to get trainset and testset. They will be in directory *dataset/dataset1/origin_data/*.

- Same for *code/data_process\*.py*

- Run *code/dim_reduction/pca* to reduce dimension. New dataset will be in *dataset/dataset1/pca*.

#### works

##### two classes
  - [x] data processing
  - [x] pca
  - [x] auto-encoder
  - [x] logistic regression 
  - [x] svm
  - [x] gbdt
  - [x] deep model
  - [x] k-NN

##### multi classes
  - [x] data processing
  - [x] pca
  - [x] auto-encoder
  - [x] logistic regression
  - [x] svm
  - [x] deep model
  - [x] k-NN
  
##### regression
  - [x] data processing
  - [x] linear regression
